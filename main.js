const movies = require("./data/movies.json");

// Questions:
// 1. Find the number of movies whose rating is greater than 8
let moviesRatingGreaterThan8 = 0;

for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    if (moviesObj.Rating > 8) {
        moviesRatingGreaterThan8++;
    }
}
console.log(moviesRatingGreaterThan8);

// 2. Create a list of movie names whose year of release is after 2000
let moviesReleasedAfter2000 = [];

for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    if (moviesObj.Year > 2000) {
        moviesReleasedAfter2000.push(moviesObj.Title);
    }
}
console.log(moviesReleasedAfter2000);

// 3. Find the top 5 highest earning movies
let highestEarningMovieObj = [];
for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    if (Number(moviesObj.Gross_Earning_in_Mil)) {
        highestEarningMovieObj.push(moviesObj.Gross_Earning_in_Mil);
    }
}

const sortedMovieEarningArr = highestEarningMovieObj.sort((a, b) => b - a);

for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    for (let iter = sortedMovieEarningArr.length; iter >= 0; iter--) {
        if (sortedMovieEarningArr[iter] === moviesObj.Gross_Earning_in_Mil) {
            console.log(moviesObj.Title);
        }
    }
}

// 4. Find the number of movies for each genre.
let moviesNumberOfEachGenre = {};
for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    if (moviesNumberOfEachGenre[moviesObj.Genre]) {
        moviesNumberOfEachGenre[moviesObj.Genre]++;
    }
    else {
        moviesNumberOfEachGenre[moviesObj.Genre] = 1;
    }
}
console.log(moviesNumberOfEachGenre);


// 5. Find the number of movies for each genre for each year
let totalMoviesOfEachGenreEachYear = {};
for (let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    let movieYear = moviesObj.Year;
    let movieGenre = moviesObj.Genre;
    console.log(movieYear);
    console.log(movieGenre);
    if (!totalMoviesOfEachGenreEachYear[movieYear]) {
        totalMoviesOfEachGenreEachYear[movieYear] = {};
    }
    if (totalMoviesOfEachGenreEachYear[movieYear][movieGenre]) {
        totalMoviesOfEachGenreEachYear[movieYear][movieGenre]++;
    }
    else {
        totalMoviesOfEachGenreEachYear[movieYear][movieGenre] = 1;
    }
}
console.log(totalMoviesOfEachGenreEachYear);

// 6. Find the average earning of a movie for each Genre
let res = {};
for(let index = 0; index < movies.length; index++) {
    let moviesObj = movies[index];
    let movieGenre = moviesObj.Genre;
    let grossEarningOfEachMovie = moviesObj.Gross_Earning_in_Mil;
    if (res[movieGenre]) {
        if(isNaN(res[movieGenre][0])){
            res[movieGenre][0] += parseInt(grossEarningOfEachMovie);
        }
        res[movieGenre][1]++;
    }
    else {
       res[movieGenre] = [];
       res[movieGenre][0] = parseInt(grossEarningOfEachMovie);
       res[movieGenre][1] = 1;
    }
}
for(let value in res) {
    let totalEarning = res[value][0];
    let count = res[value][1];
    let avg = totalEarning /count;
    res[value] = avg.toFixed(2);
}
console.log(res, "avg Each Genre");




